export const truncateAddress = (address,number) => {
    return `${address?.slice(0, number)}...${address?.slice(address?.length - number)}`;
  };