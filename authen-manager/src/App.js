import "./App.css";
import React, { useEffect } from "react";
import { Navbar, Footer } from "./components";
import { Home, Login } from "./pages";
import { Routes, Route } from "react-router-dom";
import { history } from "./history";

function App() {
  if (!localStorage.getItem("token")) {
    history.push("/login");
  } else {
    history.push("/home");
  }

  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/login" element={<Login />} />
        {localStorage.getItem("token") && (
          <Route path="/home" element={<Home />} />
        )}
      </Routes>
    </div>
  );
}

export default App;
