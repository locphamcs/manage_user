import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Text,
  ModalFooter,
  Input,
  Select,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useToast } from "@chakra-ui/react";
import { updateUser, getAllUsers } from "../../redux/actions/user";
import "./index.css";

const ModalEdit = (props) => {
  const { openModal, toggleModal, data } = props;

  const dispatch = useDispatch();

  const [state, setState] = useState({
    code: data.code,
    name: data.name,
    date: data.date,
    hometown: data.hometown,
    postion: data.postion,
    branch: data.branch,
    status: data.status,
    mode: data.mode,
  });

  const toast = useToast();

  const handleSubmit = () => {
    updateUser(data._id, state)
      .then((res) => {
        dispatch(getAllUsers());
        toggleModal();
        toast({
          title: "Success!",
          description: "Cập nhật thành công!",
          status: "success",
          duration: 4000,
          isClosable: true,
          position: "top-right",
        });
      })
      .catch((error) => {
        toast({
          title: "Error!",
          description: "Lỗi",
          status: "success",
          duration: 4000,
          isClosable: true,
          position: "top-right",
        });
      });
  };

  const handleChangeStatus = (event) => {
    setState({ ...state, status: event.target.value })
  };

  const handleChangeMode = (event) => {
    setState({ ...state, mode: event.target.value })
  };

  return (
    <Modal isOpen={openModal} onClose={toggleModal} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Sửa tài khoản</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Mã</Text>
            <Input
              value={state.code}
              onChange={(e) => setState({ ...state, code: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px", "margin-top": "15px" }}>
            <Text fontSize="sm">Tên</Text>
            <Input
              value={state.name}
              onChange={(e) => setState({ ...state, name: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px", "margin-top": "15px" }}>
            <Text fontSize="sm">Trạng thái</Text>
            <Select
              value={state.status}
              onChange={handleChangeStatus}
              style={{ "margin-top": "5px" }}
            >
              <option value="Đang làm việc">Đang làm việc</option>
              <option value="Cho nghỉ">Cho nghỉ</option>
            </Select>
          </div>
          <div style={{ "margin-bottom": "10px", "margin-top": "15px" }}>
            <Text fontSize="sm">Khen thưởng/Kỷ luật</Text>
            <Select
              value={state.mode}
              onChange={handleChangeMode}
              style={{ "margin-top": "5px" }}
            >
              <option value="Không">Không</option>
              <option value="Khen thưởng">Khen thưởng</option>
              <option value="Kỷ luật">Kỷ luật</option>
            </Select>
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Ngày sinh</Text>
            <Input
              value={state.date}
              onChange={(e) => setState({ ...state, date: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Quê quán</Text>
            <Input
              value={state.hometown}
              onChange={(e) => setState({ ...state, hometown: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Chức vụ</Text>
            <Input
              value={state.postion}
              onChange={(e) => setState({ ...state, postion: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Phòng ban</Text>
            <Input
              value={state.branch}
              onChange={(e) => setState({ ...state, branch: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            isFullWidth
            colorScheme="pink"
            variant="outline"
            size="sm"
            onClick={handleSubmit}
          >
            Lưu
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default ModalEdit;
