import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Text,
  ModalFooter,
  Input,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useToast } from "@chakra-ui/react";
import { addUser, getAllUsers } from "../../redux/actions/user";
import "./index.css";

const ModalAdd = (props) => {
  const { openModal, toggleModal } = props;

  const [state, setState] = useState({
    code: "",
    name: "",
    date: "",
    hometown: "",
    postion: "",
    branch: "",
    email: "",
    password: ""
  });

  const dispatch = useDispatch()

  const toast = useToast();

  const handleSubmit = () => {
    addUser(state)
      .then((res) => {
        dispatch(getAllUsers());
        toggleModal();
        toast({
          title: "Success!",
          description: "Thêm thành công!",
          status: "success",
          duration: 4000,
          isClosable: true,
          position: "top-right",
        });
      })
      .catch((error) => {
        toast({
          title: "Error!",
          description: "Lỗi",
          status: "success",
          duration: 4000,
          isClosable: true,
          position: "top-right",
        });
      });
  };

  return (
    <Modal isOpen={openModal} onClose={toggleModal} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Thêm tài khoản</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Mã</Text>
            <Input
              value={state.code}
              onChange={(e) => setState({ ...state, code: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px", "margin-top": "15px" }}>
            <Text fontSize="sm">Tên</Text>
            <Input
              value={state.name}
              onChange={(e) => setState({ ...state, name: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px", "margin-top": "15px" }}>
            <Text fontSize="sm">Email</Text>
            <Input
              value={state.email}
              onChange={(e) => setState({ ...state, email: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px", "margin-top": "15px" }}>
            <Text fontSize="sm">Password</Text>
            <Input
              value={state.password}
              onChange={(e) => setState({ ...state, password: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Ngày sinh</Text>
            <Input
              value={state.date}
              onChange={(e) => setState({ ...state, date: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Quê quán</Text>
            <Input
              value={state.hometown}
              onChange={(e) => setState({ ...state, hometown: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Chức vụ</Text>
            <Input
              value={state.postion}
              onChange={(e) => setState({ ...state, postion: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
          <div style={{ "margin-bottom": "10px" }}>
            <Text fontSize="sm">Phòng ban</Text>
            <Input
              value={state.branch}
              onChange={(e) => setState({ ...state, branch: e.target.value })}
              style={{ "margin-top": "5px" }}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            isFullWidth
            colorScheme="pink"
            variant="outline"
            size="sm"
            onClick={handleSubmit}
          >
            Tạo
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default ModalAdd;
