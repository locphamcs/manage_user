import React, { useState } from "react";
import "./navbar.css";
import { Link } from "react-router-dom";
import { logout } from "../../redux/actions/auth";
import { useSelector, useDispatch } from "react-redux";
import {history} from "../../history"

const Navbar = () => {
  const dispatch = useDispatch();
  const handleLogout = async (event) => {
    dispatch(logout());
    history.push("/login");
    history.go()
  };
  return (
    <div className="navbar1">
      <div className="navbar-links">
        <div className="navbar-links_logo">
          <Link to="/">
            <h1>Quản lý nhân sự</h1>
          </Link>
        </div>
      </div>
      <div className="navbar-sign">
        {localStorage.getItem("token") && (
          <button
            type="button"
            className="secondary-btn"
            onClick={handleLogout}
          >
            Đăng xuất
          </button>
        )}
      </div>
    </div>
  );
};

export default Navbar;
