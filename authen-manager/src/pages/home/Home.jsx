import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from "@chakra-ui/react";
import ReactTable from "react-table";
// import "react-table/react-table.css";

import { Button, ButtonGroup } from "@chakra-ui/react";
import "./home.css";
import ModalAdd from "../../components/modal/ModalAdd";
import { getAllUsers, deleteUser } from "../../redux/actions/user";
import { useToast } from "@chakra-ui/react";
import ModalEdit from "../../components/modaledit/ModalEdit";

const Home = () => {
  const [openModal, setOpenModal] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [data, setData] = useState("");

  const toast = useToast();

  const dispatch = useDispatch();
  const { users } = useSelector((state) => state);
  const { user } = useSelector((state) => state);

  useEffect(() => {
    dispatch(getAllUsers());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggle = (props) => {
    setOpenModal(!openModal);
  };

  const toggleEdit = (props) => {
    setOpenModalEdit(!openModalEdit);
  };

  const handleDelete = (id) => {
    console.log("delete", id);
    deleteUser(id).then((res) => {
      toast({
        title: "Success!",
        description: "Xoá thành công!",
        status: "success",
        duration: 4000,
        isClosable: true,
        position: "top-right",
      });
      dispatch(getAllUsers());
    });
  };

  const handleUpdated = (item) => {
    setData(item);
    setOpenModalEdit(!openModalEdit);
  };

  return (
    <div className="container">
      {localStorage.getItem("role") === "admin" && (
        <Button
          colorScheme="blue"
          style={{ width: "10rem", height: "30px" }}
          size="md"
          onClick={toggle}
        >
          Thêm tài khoản
        </Button>
      )}
      <TableContainer>
        <Table variant="striped">
          <TableCaption placement="top">Danh sách tài khoản</TableCaption>
          <Thead>
            <Tr>
              <Th>Mã</Th>
              <Th>Tên</Th>
              <Th>Email</Th>
              <Th>Ngày sinh</Th>
              <Th>Quê quán</Th>
              <Th>Chức vụ</Th>
              <Th>Khen thưởng/Kỷ luật</Th>
              <Th>Trạng thái</Th>
              <Th>Phòng ban</Th>
              {localStorage.getItem("role") === "admin" && <Th>Hành động</Th>}
            </Tr>
          </Thead>
          {/* <Tbody> */}
          {users
            .filter((user) => user.role === "user")
            .map((item, i) => (
              <Tr key={i}>
                <Td>{item.code}</Td>
                <Td>{item.name}</Td>
                <Td>{item.email}</Td>
                <Td>{item.date}</Td>
                <Td>{item.hometown}</Td>
                <Td>{item.postion}</Td>
                <Td>{item.mode}</Td>
                <Td>{item.status}</Td>
                <Td>{item.branch}</Td>
                {localStorage.getItem("role") === "admin" && (
                  <Td>
                    <ButtonGroup>
                      <Button
                        size="sm"
                        colorScheme="teal"
                        onClick={() => handleUpdated(item)}
                      >
                        Sửa
                      </Button>
                      <Button
                        size="sm"
                        colorScheme="red"
                        onClick={() => handleDelete(item._id)}
                      >
                        Xoá
                      </Button>
                    </ButtonGroup>
                  </Td>
                )}
              </Tr>
            ))}
          {/* </Tbody> */}
        </Table>
      </TableContainer>
      {openModal && (
        <ModalAdd openModal={openModal} toggleModal={toggle}></ModalAdd>
      )}
      {openModalEdit && (
        <ModalEdit
          openModal={openModalEdit}
          toggleModal={toggleEdit}
          data={data}
        ></ModalEdit>
      )}
    </div>
  );
};

export default Home;
