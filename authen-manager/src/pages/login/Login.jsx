import React, { useState } from "react";
import "./login.css";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { login } from "../../redux/actions/auth";
import { useToast } from "@chakra-ui/react";
import {history} from "../../history"

const Login = () => {
  const toast = useToast();

  const [state, setState] = useState({
    email: "",
    password: "",
  });
  const [validation, setValdation] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = async (e) => {
    e.preventDefault();
    await dispatch(login(state))
      .then(async (res) => {
        history.push("/home");
        history.go()
      })
      .catch((error) => {
        toast({
          title: "Error!",
          description: "Mật khẩu hoặc Email không đúng",
          status: "error",
          duration: 4000,
          isClosable: true,
          position: "bottom-right",
        });
      });
  };

  return (
    <div className="login section__padding">
      <div className="login-container">
        <h1>Đăng nhập</h1>
        <form className="login-writeForm" autoComplete="off">
          <div className="login-formGroup">
            <label>Tên đăng nhập</label>
            <input
              type="text"
              placeholder="Username"
              value={state.email}
              onChange={(e) => setState({ ...state, email: e.target.value })}
            />
          </div>
          <div className="login-formGroup">
            <label>Mật khẩu</label>
            <input
              type="password"
              placeholder="Password"
              value={state.password}
              onChange={(e) => setState({ ...state, password: e.target.value })}
            />
          </div>

          <div className="login-button">
              <button
                className="login-writeButton"
                type="submit"
                onClick={handleSubmit}
              >
                Đăng nhập
              </button>
            <Link to="/register">
              <button className="login-reg-writeButton" type="submit">
                Đăng ký
              </button>
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
