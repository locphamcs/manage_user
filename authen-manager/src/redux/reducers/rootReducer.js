import * as Types from "../../utils/Constants";
import produce from "immer";

const initialState = {
    isOpen:false,
    users:[]
};

const reducer = (state = initialState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case "LOGIN_ADMIN": {
        return {...state, user: action.user.userInfo}
      }
      case "CHANGE_ROLE": {
        return {...state, userRole: action.userRole};
      }
      case Types.GET_USERS:
        draft.users = action.users;
        break;
      default:
        return state;
    }
  });
};

export default reducer;
