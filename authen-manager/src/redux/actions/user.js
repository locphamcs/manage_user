import axiosInstance from "./../../axiosInstance";
import * as Type from "../../utils/Constants";

export const getAllUsers = async () => {
    return async dispatch => {
        try {
            let { data } = await axiosInstance.get(`/users`);
            dispatch({ type: Type.GET_USERS, users: data });
            return {
                status: true,
                data
            }
        } catch (e) {
            // toastCustom("Mất kết nối với server", "error");
            console.log("e: ", e)
        }
    }
}

export const addUser = (data) => {
    return axiosInstance.post('/users', data);
}

export const deleteUser = (id) => {
    return axiosInstance.delete(`/users/${id}`);
}

export const updateUser = (id, data) => {
    return axiosInstance.put(`/users/${id}`, data);
}
