import axiosInstance from "./../../axiosInstance";
import setAuthorizationToken from "../../utils/setAuthorizationToken";
import { setStorage } from "../../utils/helper";
import * as Type from "../../utils/Constants";

export const login = user => {
  console.log("ac: ",user)
    return async dispatch => {
      try {
        console.log("data: ")
        let {data} = await axiosInstance.post('/auth/login', user)
        console.log("data: ",data)
        dispatch({ type: Type.LOGIN_ADMIN, user: data });
        localStorage.setItem("token", data.token);
        localStorage.setItem("role", data.userInfo.role);
        dispatch(setToken(data.token, null));
        return {
          status: true,
          data
        }
      } catch (e) {
        throw (e)
      }
    }
}
  
  export function setToken(token, user) {
    return dispatch => {
      setStorage('token', token, parseInt(180 * 60 * 6));
      setAuthorizationToken(token);
    }
  }
  
  export function logout() {
    return dispatch => {
      localStorage.removeItem('token');
      localStorage.removeItem('token_expiresIn');
      localStorage.removeItem('role');
      delete axiosInstance.defaults.headers.common["Authorization"];
    }
  }
  
//   export function setCurrentUser(user) {
//     return {
//       type: "CLEAR_STORE",
//       payload: {
//         data: user
//       }
//     };
//   }