import axiosInstance from "./../../axiosInstance";
import * as Type from "../../utils/Constants";

export const getAllNfts = async (page, limit) => {
  return async dispatch => {
    try {
      let {data} = await axiosInstance.get(`/nft?page=${page}&limit=${limit}`);
      dispatch({type: Type.GET_NFTS, nfts: data});
      return {
        status: true,
        data
      }
    } catch (e) {
      // toastCustom("Mất kết nối với server", "error");
      console.log("e: ", e)
    }
  }
}

export const addNft = (data) => {
  return axiosInstance.post("/nft/mint", data);
};