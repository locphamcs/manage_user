import axiosInstance from "./../../axiosInstance";

const config = {headers: {"Content-Type": "multipart/form-data"}};

export const addIpfs = (data) => {
  return axiosInstance.post("/ipfs/upload", data, config);
};