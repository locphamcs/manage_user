module.exports = {
  apps: [{
    name: 'BURIBURI_API',
    script: 'app.js',
    instances: 'max',
    exec_mode: 'cluster',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy: {
    prod: {
      user: 'ubuntu',
      host: '13.125.206.5',
      ref: 'origin/master',
      repo: 'git@gitlab.com:vudoancs/buriburi-server.git',
      path: '/home/ubuntu/buriburi-prod',
      'pre-deploy': 'git add . && git reset --hard',
      'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js --env production'
    },
    dev: {
      user: 'ubuntu',
      host: '13.125.206.5',
      ref: 'origin/master',
      repo: 'git@gitlab.com:vudoancs/buriburi-server.git',
      path: '/home/ubuntu/buriburi-dev',
      'pre-deploy': 'git add . && git reset --hard',
      'post-deploy': 'npm install && npm run apidoc && pm2 startOrRestart ecosystem.config.js'
    }
  }
};
