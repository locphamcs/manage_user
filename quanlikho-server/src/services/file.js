import { getFileInfo } from '../common/utils';

var File = require('../models/file')

const checkFileFromClient = async(image)=>{
    const  {fileName, fileUrl} = getFileInfo(image)
          if (fileName) {
            return File.findOne({ file: fileName }).then(item => {
              if (!item) {
                return Promise.reject(fileName + ' not found');
              }else{
                return Promise.resolve();
              }
            });
          }else{
            return Promise.resolve();
          }
}

export default {
    checkFileFromClient
}