var Todolists = require('../models/todolists')
    , Constants = require('../common/constants')
    , FileUtils = require('../common/file_utils')
require("mongoose-pagination")
import _ from 'lodash'
import { query } from 'express';
var mongoose = require('mongoose');


const ALLOWED_ATTRIBUTES = ["type", "todos","date"];

const add = async (req, res, next) => {
  try {
    let body = _.pick(req.body, ALLOWED_ATTRIBUTES);
    let model = new Todolists(body);
    const item = await model.save();
    res.json(item);
  } catch (error) {
    next(error);
  }
};

const remove = async (req, res, next) => {
  try {
    await Todolists.findByIdAndDelete(req.params.id);
    res.json({success: true, messaging:"Xoá danh sách công việc thành công"});
  } catch (error) {
    next(error);
  }
};

const editCheck = async (req, res, next) => {
  try {
    let todos = req.body.todos;
    await todos.forEach(function (item) {
      Todolists.findById(req.params.id).then(object => {
        let todo = object.todos.id(item._id);
        todo.checked = item.checked;
        return object.save();
    });
  });
    let body = await Todolists.findById(req.params.id);
    res.json(body);
  } catch (error) {
    next(error);
  }
};

const edit = async (req, res, next) => {
  try {
    let todos = req.body.todos;
    await todos.forEach(function (item) {
      Todolists.findById(req.params.id).then(object => {
        let todo = object.todos.id(item._id);
        todo.did = item.did;
        return object.save();
    });
  });
    let body = await Todolists.findById(req.params.id);
    res.json(body);
  } catch (error) {
    next(error);
  }
};

const index = async (req, res, next) => {
  try {
    var query = req.query;
    Todolists.find(query, function(err, todos) {
      res.send(todos);  
    });
  } catch (error) {
    next(error);
  }
};



export default {
    add,
    remove,
    editCheck,
    edit,
    index,
}