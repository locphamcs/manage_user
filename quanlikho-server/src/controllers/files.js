var express = require('express')
    , router = express.Router()
    , File = require('../models/file')
    , Constants = require('../common/constants')
    , FileUtils = require('../common/file_utils')
require("mongoose-pagination")
import _ from 'lodash'
import { auth } from '../middlewares'
import { response,randomFileNameByExtension } from '../common/utils';
var multer = require('multer')
var path = require('path')

var uploadImageFile = multer({ 
    storage: multer.memoryStorage(), 
    limits: { fileSize: Constants.LimitImageSize*1000000 },
    fileFilter:  (req, file, cb) =>{
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    
        if (mimetype && extname) {
          return cb(null, true);
        }
        cb(new Error("Error: File upload only supports the following filetypes - " + filetypes));
      }
    }).single('file')


const uploadImage = (req, res, next) => {
    uploadImageFile(req, res, function (err) {
        if (err) {
            next(err)
        } else {
            if (req.file) {
                FileUtils.uploadFileToStorage(req.file)
                    .then((fileName) => {
                        if (fileName) {
                            var file = new File({ file: fileName, type: Constants.MediaType.Image })
                            file.save((err, item) => {
                                if (err) return next(err)
                                res.status(200).json(item.file)
                            })
                        }
                    })
                    .catch(next)
            } else {
                res.status(400).json(response(false, {}, __("Empty file")))
            }
        }
    })
}

export default {
    uploadImage
}