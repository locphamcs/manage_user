const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var User = require("../models/user"),
  Constants = require("../common/constants"),
  FileUtils = require("../common/file_utils");
require("mongoose-pagination");
import _ from "lodash";
import moment from "moment";
import { response, sendMail, sendSMS } from "../common/utils";
var randomstring = require("randomstring");
var mongoose = require("mongoose");
const { ObjectId } = mongoose.Types;
import Services from "../services/index";

function loginUser(userId, res) {
  const generateToken = (user, res) => {
    var token = jwt.sign(
      { id: user._id, email: user.email },
      Constants.JWTSecret
    );
    res.json({ token, userInfo: user.toObject() });
  };
  User.findById(userId)
    .populate("group")
    .exec((err, user) => {
      if (err) return res.status(500).json(err);
      generateToken(user, res);
    });
}

const sendCodePhone = (req, res, next) => {
  const verifyCode = Math.floor(1000 + Math.random() * 9000);
  sendSMS(req.body.phone, verifyCode)
    .then((response) => {
      User.findOne(
        { phone: req.body.phone, authType: Constants.AuthType.Email },
        (err, user) => {
          if (err) return next(err);
          if (user) {
            user.verifyCode = verifyCode;
            user.save((err, item) => {
              if (err) return next(err);
              res.json({ success: true });
            });
          } else {
            var user = new User({
              phone: req.body.phone,
              verifyCode,
            });
            user.save((err, item) => {
              if (err) return next(err);
              res.json({ success: true });
            });
          }
        }
      );
    })
    .catch((err) => {
      const response = err.response;
      if (response && response.data.message) {
        res.status(400).json({ message: response.data.message });
      } else {
        next(err);
      }
    });
};

const register = async (req, res, next) => {
  try {
    const body = _.pick(req.body, ["email", "password", "role", "avatar"]);
    let model = new User(body);
    const item = await model.save();
    res.json(item);
  } catch (error) {
    next(error);
  }
};

const socialLogin = (req, res, next) => {
  User.findOne(
    { socialId: req.body.socialId, authType: req.body.authType },
    (err, user) => {
      if (err) return next(err);
      const body = _.pick(req.body, [
        "name",
        "nickname",
        "email",
        "phone",
        "socialId",
        "authType",
        "avatar",
      ]);
      if (user) {
        User.update({ _id: user._id }, { $set: body }, (err) => {
          loginUser(user._id, res);
        });
      } else {
        var hash = bcrypt.hashSync(process.env.DEFAULT_SOCIAL_PASSWORD, 10);
        var user = new User({
          ...body,
          hash,
          isRegistered: true,
        });
        user.save((err, user) => {
          if (err) return next(err);
          loginUser(user._id, res);
        });
      }
    }
  );
};

const login = (req, res) => {
  console.log("login");
  User.findOne({ email: req.body.email }, (err, user) => {
    console.log("user: ", user)
    if (req.body.password !== user.password) {
      console.log("login1");
      res.status(400).send(response(false, {}, __("Verify Code is incorrect")));
    } else {
    console.log("login2");
      loginUser(user._id, res);
    }
  });
};

const findEmail = async (req, res, next) => {
  try {
    var user = await User.findOne({
      phone: req.body.phone,
      verifyCode: req.body.code,
    });
    if (user) {
      res.json({ email: user.email });
    } else {
      res.status(400).send(response(false, {}, __("Verify Code is incorrect")));
    }
  } catch (error) {
    next(error);
  }
};

const findPassword = async (req, res, next) => {
  try {
    var user = await User.findOne({
      phone: req.body.phone,
      verifyCode: req.body.code,
    });
    if (user) {
      const newPass = randomstring.generate(7);
      const content = "Your new password: <b>" + newPass + "</b>";
      sendMail(
        user.email,
        content,
        () => {
          var hash = bcrypt.hashSync(newPass, 10);
          User.update({ _id: user._id }, { $set: { hash } }, (err) => {
            if (err) return next(err);
            res.json({ success: true });
          });
        },
        "[BuriBuri] Reset Password"
      );
    } else {
      res.status(400).send(response(false, {}, __("Verify Code is incorrect")));
    }
  } catch (error) {
    next(error);
  }
};

export default {
  sendCodePhone,
  register,
  socialLogin,
  login,
  findEmail,
  findPassword,
};
