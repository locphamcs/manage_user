var Group = require('../models/group')
var User  = require('../models/user')
require("mongoose-pagination")
import _ from 'lodash'


const ALLOWED_ATTRIBUTES = ['name', "description"];
const SEARCH_ATTRIBUTES = ["search", "page", "limit"];

const add = async (req, res, next) => {
  try {
    let body = _.pick(req.body, ALLOWED_ATTRIBUTES); 
    let model = new Group(body);
    const item = await model.save();
    res.json(item);
  } catch (error) {
    next(error);
  }
};

const index = async (req, res, next) => {
  let query = {};
  const body = _.pick(req.query, SEARCH_ATTRIBUTES);
  if (body.type) {
    query.type = body.type;
  }
  const page = body.page ? parseInt(body.page) : 1;
  const limit = body.limit ? parseInt(body.limit) : 10;
  if (body.search) {
    query = {
      $and: [
        query,
        {
          $or: [{ name: { $regex: body.search, $options: "i" } }],
        },
      ],
    };
  }
  Group.find(query)
    .sort({ createdAt: -1 })
    .paginate(page, limit, (err, docs, total) => {
      if (err) return next(err);
      res.json({ data: docs, total });
    });
}; 

const remove = async (req, res, next) => {
    try {
      await Group.remove({ _id: req.params.id });
      res.json({success: true});
    } catch (error) {
      next(error);
    }
};

const addMember = async (req, res, next) => {
    try {
      const group = await Group.findById(req.params.id)
      group.listUser = [...group.listUser, ...req.body]
      console.log(group);
      const item = await group.save();
      res.json(item);
    } catch (error) {
      next(error);
    }
};

const edit = async (req, res, next) => {
  try {
    let body = _.pick(req.body, ALLOWED_ATTRIBUTES);
    await Group.updateOne({ _id: req.params.id }, { $set: body });
    res.json({ success: true});
  } catch (error) {
    next(error);
  }
};



export default {
    add,
    index,
    addMember,
    remove,
    edit,
}