var Question = require('../models/question')
require("mongoose-pagination")
import _ from 'lodash'


const ALLOWED_ATTRIBUTES = ["question", "type","answer","options"];
const SEARCH_ATTRIBUTES = ["search", "page", "limit"];

const add = async (req, res, next) => {
  try {
    let body = _.pick(req.body, ALLOWED_ATTRIBUTES);
    let model = new Question(body);
    const item = await model.save();
    res.json(item);
  } catch (error) {
    next(error);
  }
};

const remove = async (req, res, next) => {
    try {
      await Question.findByIdAndDelete(req.params.id);
      res.json({success: true, messaging:"Xoá câu hỏi thành công"});
    } catch (error) {
      next(error);
    }
};

const edit = async (req, res, next) => {
    try {
      let body = _.pick(req.body, ALLOWED_ATTRIBUTES);
      await Question.findByIdAndUpdate(req.params.id, body)
      res.json(body);
    } catch (error) {
      next(error);
    }
};

const index = async (req, res, next) => {
  let query = {};
  const body = _.pick(req.query, SEARCH_ATTRIBUTES);
  if (body.type) {
    query.type = body.type;
  }
  const page = body.page ? parseInt(body.page) : 1;
  const limit = body.limit ? parseInt(body.limit) : 10;
  if (body.search) {
    query = {
      $and: [
        query,
        {
          $or: [{ name: { $regex: body.search, $options: "i" } }],
        },
      ],
    };
  }
  Question.find(query)
    .sort({ createdAt: -1 })
    .paginate(page, limit, (err, docs, total) => {
      if (err) return next(err);
      res.json({ data: docs, total });
    });
};

export default {
    index,
    add,
    edit,
    remove,
}