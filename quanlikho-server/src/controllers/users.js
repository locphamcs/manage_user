const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var User = require("../models/user"),
  Constants = require("../common/constants"),
  FileUtils = require("../common/file_utils");
require("mongoose-pagination");
import _ from "lodash";
import moment from "moment";
import { response, sendMail, sendSMS } from "../common/utils";
var randomstring = require("randomstring");
var mongoose = require("mongoose");
const { ObjectId } = mongoose.Types;
import Services from "../services/index";

const ALLOWED_ATTRIBUTES = [
  "code",
  "name",
  "date",
  "hometown",
  "branch",
  "postion",
  "email",
  "password"
];

const ALLOWED_ATTRIBUTES_EDIT = [
  "code",
  "name",
  "date",
  "hometown",
  "branch",
  "postion",
  "status",
  "mode"
];

const add = async (req, res, next) => {
  try {
    const hash = bcrypt.hashSync(req.body.password, 10);
    const body = _.pick(req.body, ALLOWED_ATTRIBUTES);
    const user = new User({
      ...body,
      hash,
      role: "user",
      enable: true,
    });
    const item = await user.save();
    res.json({ success: true, user: item });
  } catch (error) {
    next(error);
  }
};
const index = async (req, res, next) => {
  User.find({}).then(function (users) {
    res.json(users);
  });
};

const edit = async (req, res, next) => {
  try {
    const body = _.pick(req.body, ALLOWED_ATTRIBUTES_EDIT);
    const user = await User.findOne({ _id: req.params.id });
    if (user.email !== body.email) {
      const item = await User.findOne({ email: body.email });
      if (item) {
        return res
          .status(400)
          .send(response(false, {}, "Email đã được đăng ký"));
      }
    }
    if (user.username !== body.username) {
      const item = await User.findOne({ username: body.username });
      if (item) {
        return res
          .status(400)
          .send(response(false, {}, "username đã được đăng ký"));
      }
    }
    await User.updateOne({ _id: req.params.id }, { $set: body });
    res.json({ success: true });
  } catch (error) {
    next(error);
  }
};
const remove = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    if (user) {
      await User.findByIdAndUpdate(user.team, {
        $pull: { members: req.params.id },
      });
      await User.remove({ _id: req.params.id });
    }
    res.json({ success: true });
  } catch (error) {
    next(error);
  }
};

export default {
  add,
  index,
  edit,
  remove,
};
