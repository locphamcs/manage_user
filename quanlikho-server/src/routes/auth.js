const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var express = require('express')
    , router = express.Router()
import Validation from '../validations/index'
import { admin, auth, validator } from '../middlewares'
import AuthCtrl from '../controllers/auth'

/**
* @api {post} /auth/code Send verify code via phone
* @apiVersion 1.0.0
* @apiName Send verify code via phone
* @apiGroup Auth
* 
* @apiParam {String} phone
*
* @apiSuccess {String} token
* 
*/
router.post("/code", Validation.auth.validateSendCodePhone, validator, AuthCtrl.sendCodePhone);

/**
* @api {post} /auth/register Register
* @apiVersion 1.0.0
* @apiName Register
* @apiGroup Auth
* 
* @apiParam {String} name
* @apiParam {String} email
* @apiParam {String} password
* @apiParam {String} username
* @apiParam {String} role
*
* @apiSuccess {Boolean} success
* 
*/
router.post("/register", Validation.auth.validateRegister, validator, AuthCtrl.register);

/**
* @api {post} /auth/social_login Social login
* @apiVersion 1.0.0
* @apiName Social login
* @apiGroup Auth
* 
* @apiParam {String} [name]
* @apiParam {String} [nickname]
* @apiParam {String} [email]
* @apiParam {String} [phone]
* @apiParam {String} socialId
* @apiParam {String} authType kakao, naver, facebook
* @apiParam {String} [avatar]
*
* @apiSuccess {Object} userInfo
* @apiSuccess {String} token
* 
*/
router.post("/social-login", Validation.auth.validateSocialLogin, validator, AuthCtrl.socialLogin);


/**
* @api {post} /auth/login Login
* @apiVersion 1.0.0
* @apiName Login
* @apiGroup Auth
* 
* @apiParam {String} email
* @apiParam {String} password
* 
* @apiSuccess {Object} userInfo
* @apiSuccess {String} token
* 
*/
router.post("/login", Validation.auth.validateLogin, validator, AuthCtrl.login);

/**
* @api {post} /auth/forgot/code Send verify code via phone
* @apiVersion 1.0.0
* @apiName Send verify code via phone
* @apiGroup Auth
* 
* @apiParam {String} phone
*
* @apiSuccess {String} token
* 
*/
router.post("/forgot/code", Validation.auth.validateSendCodePhoneForForgot, validator, AuthCtrl.sendCodePhone);

/**
* @api {post} /auth/find-email Find email
* @apiVersion 1.0.0
* @apiName Find email
* @apiGroup Auth
* 
* @apiParam {String} phone
* @apiParam {String} code
*
* @apiSuccess {String} email
* 
*/
router.post("/find-email", Validation.auth.validateFind, validator, AuthCtrl.findEmail);

/**
* @api {post} /auth/find-password Find password
* @apiVersion 1.0.0
* @apiName Find password
* @apiGroup Auth
* 
* @apiParam {String} phone
* @apiParam {String} code
*
* @apiSuccess {Boolean} success
* 
*/
router.post("/find-password", Validation.auth.validateFind, validator, AuthCtrl.findPassword);

module.exports = router