var express = require("express"),
  router = express.Router();
import Validation from "../validations/index";
import { auth, validator,manager } from "../middlewares";
import TodosCtrl from "../controllers/todolists";

/**
 * @api {post} /todolists Add Todolist (Gửi list công việc)
 * @apiVersion 1.0.0
 * @apiName Add Todo
 * @apiGroup Todo list
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {String} type today or month
 * @apiParam {Array} todos
 * @apiParam {String} todo Mô tả công việc
 * @apiParam {Boolean} did trạng thái hoàn thành, mặc định false
 */
router.post("/", validator, auth, TodosCtrl.add);

/**
 * @api {delete} /todolists/:id Delete Todo list
 * @apiVersion 1.0.0
 * @apiName Delete Todo list
 * @apiGroup Todo list
 *
 * @apiHeader Authorization Bearer token
 *
 */
router.delete("/:id", validator ,auth, TodosCtrl.remove)

/**
 * @api {put} /todolists/:id/check Edit Todo list (Manager kiểm tra công việc user)
 * @apiVersion 1.0.0
 * @apiName Edit Todo list (Manager kiểm tra công việc user)
 * @apiGroup Todo list
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {Array} todos
 * @apiParam {String} _id _id của todo
 * @apiParam {Boolean} checked trạng thái hoàn thành, mặc định false
 *
 */
router.put("/:id/check",validator, manager, TodosCtrl.editCheck);

/**
 * @api {put} /todolists/:id Edit Todo list (Update trạng thái công việc của user)
 * @apiVersion 1.0.0
 * @apiName Edit Todo list (Update trạng thái công việc của user)
 * @apiGroup Todo list
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {Array} todos
 * @apiParam {String} _id _id của todo
 * @apiParam {Boolean} did trạng thái hoàn thành, mặc định false
 *
 */
router.put("/:id",validator,auth, TodosCtrl.edit);

/**
 * @api {get} /todolists Get todolists
 * @apiVersion 1.0.0
 * @apiName Get todolist
 * @apiGroup Todo list
 *
 * @apiParam {String} type today hoac month
 * @apiParam {String} [search]
 * @apiParam {Number} [page] default 1
 * @apiParam {Number} [limit] default 10
 */
router.get("/",validator,auth, TodosCtrl.index)



module.exports = router