var express = require("express"),
  router = express.Router();
import Validation from "../validations/index";
import { validator,manager,auth } from "../middlewares";
import QuestionCtrl from "../controllers/question";

/**
 * @api {post} /questions Add Question (Manager)
 * @apiVersion 1.0.0
 * @apiName Add Question
 * @apiGroup Question
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {String} type text hoặc multipleChoice
 * @apiParam {String} question nội dung câu hỏi
 * @apiParam {String} [options] Danh sách các lựa chọn
 * @apiParam {String} answer Thứ tự câu trả lời đúng hoặc Đáp án nếu đó là câu hỏi Text
 *
 */
router.post("/", Validation.question.validateAddQuestion, manager, validator, QuestionCtrl.add);

/**
 * @api {delete} /questions/:id Delete Question
 * @apiVersion 1.0.0
 * @apiName Delete Question
 * @apiGroup Question
 *
 * @apiHeader Authorization Bearer token
 *
 */
router.delete("/:id", manager, QuestionCtrl.remove)

/**
 * @api {put} /questions/:id Edit Question
 * @apiVersion 1.0.0
 * @apiName Edit Question
 * @apiGroup Question
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {String} type text hoặc multipleChoice
 * @apiParam {String} question nội dung câu hỏi
 * @apiParam {String} [options] Danh sách các lựa chọn
 * @apiParam {String} answer Thứ tự câu trả lời đúng hoặc Đáp án nếu đó là câu hỏi Text
 *
 */
router.put("/:id",manager, validator, QuestionCtrl.edit);

/**
 * @api {get} /questions Get Question
 * @apiVersion 1.0.0
 * @apiName Get Question
 * @apiGroup Question
 *
 * @apiParam {String} [search]
 * @apiParam {Number} [page] default 1
 * @apiParam {Number} [limit] default 10
 */
router.get("/",auth, QuestionCtrl.index)


module.exports = router