var express = require("express"),
  router = express.Router();
import Validation from "../validations/index";
import { auth, validator } from "../middlewares";
import GroupCtrl from "../controllers/group";
import GroupMiddlewares from "../middlewares/group";

/**
 * @api {post} /group Add Group
 * @apiVersion 1.0.0
 * @apiName Add Group
 * @apiGroup Group
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {String} name
 * @apiParam {String} description
 *
 */
router.post("/", Validation.group.validateAdd, validator, GroupMiddlewares.add, GroupCtrl.add);

//GroupMiddlewares.add

/**
 * @api {post} /group/addMember/:id Add Member
 * @apiVersion 1.0.0
 * @apiName Add Member
 * @apiGroup Group
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {String} userId
 *
 */
router.post("/addMember/:id",Validation.group.validateAddMemberGroup, validator, GroupMiddlewares.addMember, GroupCtrl.addMember)

/**
 * @api {put} /group/:id Edit Group
 * @apiVersion 1.0.0
 * @apiName Edit Group
 * @apiGroup Group
 *
 * @apiHeader Authorization Bearer token
 *
 * @apiParam {String} name
 * @apiParam {String} description
 *
 */
router.put("/:id",Validation.group.validateEdit,validator, GroupMiddlewares.edit, GroupCtrl.edit);

/**
 * @api {delete} /group/:id Delete Group
 * @apiVersion 1.0.0
 * @apiName Delete Group
 * @apiGroup Group
 *
 * @apiHeader Authorization Bearer token
 *
 */
router.delete("/:id",Validation.group.validateDelete ,validator, GroupMiddlewares.remove, GroupCtrl.remove)

/**
 * @api {get} /group Get Group
 * @apiVersion 1.0.0
 * @apiName Get Group
 * @apiGroup Group
 *
 * @apiParam {String} [search]
 * @apiParam {Number} [page] default 1
 * @apiParam {Number} [limit] default 10
 */
router.get("/",Validation.group.validateGet,validator,GroupMiddlewares.index, GroupCtrl.index)


module.exports = router