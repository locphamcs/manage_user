var path = require("path")
var express = require('express')
    , router = express.Router()

router.use('/auth', require('./auth'))
router.use('/files', require('./files'))
router.use('/users', require('./users'))
router.use('/todolists', require('./todolists'))
router.use('/group', require('./group'))
router.use('/questions', require('./questions'))
module.exports = router