var express = require('express')
    , router = express.Router()
import { admin, auth, validator } from '../middlewares'
import FileCtrl from '../controllers/files'

/**
* @api {post} /files/image Upload image
* @apiVersion 1.0.0
* @apiName Upload image
* @apiGroup Files
* 
* @apiHeader Authorization Bearer token
*
* @apiParam {File} file
*
* @apiSuccess {String} imageName
* 
*/
router.post("/image",auth, FileCtrl.uploadImage);

module.exports = router