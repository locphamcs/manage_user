const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var express = require("express"),
  router = express.Router();
import Validation from "../validations/index";
import { admin, auth, validator } from "../middlewares";
import UsersCtrl from "../controllers/users";

// router.put(
//   "/",
//   Validation.user.validateEditProfile,
//   validator,
//   auth,
//   UsersCtrl.editProfile
// );

router.post("/", Validation.user.validateAdd, validator, [], UsersCtrl.add);

router.put("/:id", Validation.user.validateAdd, validator, [], UsersCtrl.edit);

router.delete(
  "/:id",
  Validation.user.validateAdd,
  validator,
  [],
  UsersCtrl.remove
);

router.get('/', Validation.user.validateAdd, validator, [], UsersCtrl.index);


module.exports = router;
