const {
  body,
  check,
  sanitizeBody,
  param,
  query,
} = require("express-validator");
import Constants from "../common/constants";
import File from "../models/file";
import User from "../models/user";
import Services from "../services/index";

export default {
  validateSendCodePhone: [
    check("phone")
      .exists()
      .withMessage("phone is required")
      .custom((value) => {
        return User.findOne({
          phone: value,
          authType: Constants.AuthType.Email,
          isRegistered: true,
        }).then((user) => {
          if (user) {
            return Promise.reject(__("Phone is registered"));
          }
        });
      }),
  ],
  validateRegister: [
    check("email")
      .exists()
      .withMessage("email is required")
      .isEmail()
      .withMessage(__("The Email Address is in an invalid format."))
      .custom((value) => {
        return User.findOne({
          email: value,
          authType: Constants.AuthType.Email,
          isRegistered: true,
        }).then((user) => {
          if (user) {
            return Promise.reject(__("Email is registered"));
          }
        });
      }),
    check("password")
      .exists()
      .withMessage("password is required")
      .notEmpty()
      .withMessage("password is not blank"),
      // .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i")
      // .withMessage("password Incorrect format")
      
  ],
  validateLogin: [
    
  ],
  validateSocialLogin: [
    check("email")
      .optional()
      .isEmail()
      .withMessage(__("The Email Address is in an invalid format.")),
    check("socialId").exists().withMessage("socialId is required"),
    check("authType")
      .optional()
      .custom((value) => Object.values(Constants.AuthType).indexOf(value) > -1)
      .withMessage("Invalid authType"),
    check("name").optional().notEmpty().withMessage("name is not blank"),
    check("phone").optional().notEmpty().withMessage("phone is not blank"),
    check("avatar").optional().notEmpty().withMessage("avatar is not blank"),
    check("nickname")
      .optional()
      .notEmpty()
      .withMessage("nickname is not blank"),
  ],
  validateSendCodePhoneForForgot: [
    check("phone")
      .exists()
      .withMessage("phone is required")
      .custom((value) => {
        return User.findOne({
          phone: value,
          authType: Constants.AuthType.Email,
          isRegistered: true,
        }).then((user) => {
          if (!user) {
            return Promise.reject(__("The phone hasn't registered yet"));
          }
        });
      }),
  ],
  validateFind: [
    check("phone")
      .exists()
      .withMessage("phone is required")
      .custom((value) => {
        return User.findOne({
          phone: value,
          authType: Constants.AuthType.Email,
          isRegistered: true,
        }).then((user) => {
          if (!user) {
            return Promise.reject(__("The phone hasn't registered yet"));
          }
        });
      }),
    check("code").exists().withMessage("code is required"),
  ],
};
