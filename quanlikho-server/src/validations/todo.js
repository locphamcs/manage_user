const { body, check, sanitizeBody,param,query } = require('express-validator');
import Constants from '../common/constants'
import Todo from '../models/todolists'
import Services from '../services/index'

export default {
  ValidateAdd: [
   check('name').exists().withMessage("name is required"),
  check('description').exists().withMessage("description is required"),
  ],
  
}
