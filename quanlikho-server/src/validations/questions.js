const { body, check, sanitizeBody,param,query } = require('express-validator');
import Question from '../models/question'
import { validator } from '../middlewares';

export default {
  validateAddQuestion: [
    check('question').exists().withMessage('question is required').notEmpty().withMessage("question is not blank").custom(value => {
      return Question.findOne({ question: value}).then(question => {
        if (question) {
          return Promise.reject(__('The question already exists'));
        }
      })
    }),
    check('type').exists().withMessage('type is required').notEmpty().withMessage('type is not blank'),
    check('answer').exists().withMessage('answer is required').notEmpty().withMessage('answer is not blank'),
  ],
}
