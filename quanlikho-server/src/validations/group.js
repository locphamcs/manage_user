const { body, check, sanitizeBody,param,query } = require('express-validator');
import Group from '../models/group'

export default {
  validateAdd: [
    check('name').exists().withMessage('name is required').notEmpty().withMessage("name is not blank").custom(value => {
      return Group.findOne({ name: value}).then(item => {
        if (item) {
          return Promise.reject(value + " already registered with this name");
        }
      })
    }),
    check('description').exists().withMessage('description is required').notEmpty().withMessage('description is not blank'),
  ],
  //Check xem trong, dung du lieu, email
  validateEdit: [
    param("id")
      .exists()
      .withMessage("id not found")
      .custom((value) => {
        return Group.findOne({ _id: value }).then((item) => {
          if (!item) {
            return Promise.reject(value + " not found");
          }
        });
      }),
    check("name")
      .exists()
      .withMessage("name is required")
      .custom((value) => {
        return Group.findOne({ name: value }).then((item) => {
          if (item) {
            return Promise.reject(value + " already registered with this name");
          }
        });
      }),
      check('description')
      .exists()
      .withMessage('description is required')
      .notEmpty()
      .withMessage('description is not blank'),
  ],
  validateAddMemberGroup:[
    param("id")
      .exists()
      .withMessage("id not found")
      .custom((value) => {
        return Group.findOne({ _id: value }).then((item) => {
          if (!item) {
            return Promise.reject(value + " not found");
          }
        });
      }),
  ],
  validateDelete: [
    param("id")
      .exists()
      .withMessage("id not found")
      .custom((value) => {
        return Group.findOne({ _id: value }).then((item) => {
          if (!item) {
            return Promise.reject(value + " not found");
          }
        });
      }),
  ],
  validateGet: [
    query("page")
      .optional()
      .isInt([{ min: 1 }])
      .withMessage("Invalid page"),
    query("limit")
      .optional()
      .isInt([{ min: 1 }])
      .withMessage("Invalid limit"),
  ],
}
