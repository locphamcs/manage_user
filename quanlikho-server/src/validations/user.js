const { body, check, sanitizeBody,param,query } = require('express-validator');
import Constants from '../common/constants'
import File from '../models/file'
import User from '../models/user'
import Services from '../services/index'

export default {
  validateSendCodePhone: [
    check('phone').exists().withMessage("phone is required").notEmpty().withMessage('phone is not blank'),
  ],
  validateEditProfile: [
    check('email').exists().withMessage('email is required').isEmail().withMessage(__("The Email Address is in an invalid format.")),
    check('password').optional().notEmpty().withMessage('password is not blank'),
    check('name').optional().notEmpty().withMessage('name is not blank'),
    check('address').optional().notEmpty().withMessage('address is not blank'),
    body('avatar').optional().custom(value => {
      return Services.file.checkFileFromClient(value)
    }),
    check('phone').optional().notEmpty().withMessage('phone is not blank'),
    check('code').optional().notEmpty().withMessage('code is not blank'),
  ],
  validateAdd: [],
}
