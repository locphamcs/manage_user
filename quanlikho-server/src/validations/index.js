import auth from './auth'
import user from './user'
import todo from './todo'
import group from './group'
import question from './questions'
export default {
    auth,
    user,
    todo, 
    group, 
    question
}