var passport = require('passport');
import { response } from '../common/utils'

module.exports = function (req, res, next) {
    passport.authenticate('Bearer', function (err, user, info) {
        if (err) return next(err)

        if (user && user.enable) {
            req.userId = user._id
            req.userInfo = user
            next()
        } else {
            res.status(401).send(response(false, null, __('Invalid credentials')));
        }
    })(req, res, next);
}