var passport = require('passport');
var Constants = require('../common/constants')
import { response } from '../common/utils'

module.exports = function (req, res, next) {
    passport.authenticate('Bearer', function (err, user, info) {
        if (err) return res.status(500).send(err)

        if (user && user.enable) {
            if (user.role == Constants.Role.Manager) {
                req.userId = user._id
                req.userInfo = user
                next()
            } else {
                res.status(401).send(response(false, null, __('No Permission')));
            }
        } else {
            res.status(401).send(response(false, null, __('Invalid credentials')));
        }
    })(req, res, next);
}