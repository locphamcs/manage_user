export const auth = require('./auth')
export const admin = require('./admin')
export const validator = require('./validator')
export const manager = require('./manager')