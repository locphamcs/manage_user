var passport = require('passport');
var Constants = require('../common/constants');
import { response } from '../common/utils';

const add = async (req, res, next) => {
  passport.authenticate('Bearer', function (err, user, info) {
    if (err) return next(err);
    if (user && user.enable) {
      if (user.role == Constants.Role.Manager) {
        req.userId = user._id;
        req.userInfo = user;
        next();
      } else {
        res.status(401).send(response(false, null, 'No Permission'));
      }
    } else {
      res.status(401).send(response(false, null, 'Invalid credentials'));
    }
  })(req, res, next);
};

const addMember = async (req, res, next) => {
  passport.authenticate('Bearer', function (err, user, info) {
    if (err) return next(err);
    if (user && user.enable) {
      if (user.role == Constants.Role.Manager) {
        req.userId = user._id;
        req.userInfo = user;
        next();
      } else {
        res.status(401).send(response(false, null, 'No Permission'));
      }
    } else {
      res.status(401).send(response(false, null, 'Invalid credentials'));
    }
  })(req, res, next);
};

const edit = async (req, res, next) => {
  passport.authenticate('Bearer', function (err, user, info) {
    if (err) return next(err);
    if (user && user.enable) {
      if (
        user.role == Constants.Role.Manager
      ) {
        req.userId = user._id;
        req.userInfo = user;
        next();
      } else {
        res.status(401).send(response(false, null,'No Permission'));
      }
    } else {
      res.status(401).send(response(false, null,'Invalid credentials'));
    }
  })(req, res, next);
};
const remove = async (req, res, next) => {
  passport.authenticate('Bearer', function (err, user, info) {
    if (err) return next(err);
    if (user && user.enable) {
      if (user.role == Constants.Role.Manager) {
        req.userId = user._id;
        req.userInfo = user;
        next();
      } else {
        res.status(401).send(response(false, null, 'No Permission'));
      }
    } else {
      res.status(401).send(response(false, null, 'Invalid credentials'));
    }
  })(req, res, next);
};
const index = async (req, res, next) => {
  passport.authenticate('Bearer', function (err, user, info) {
    if (err) return next(err);
    if (user && user.enable) {
      if (
        user.role == Constants.Role.Manager ||
        user.role == Constants.Role.User
      ) {
        req.userId = user._id;
        req.userInfo = user;
        next();
      } else {
        res.status(401).send(response(false, null, 'No Permission'));
      }
    } else {
      res.status(401).send(response(false, null, 'Invalid credentials'));
    }
  })(req, res, next);
};
export default {
  add,
  edit,
  remove,
  index,
  addMember,
};