var path = require("path")
var i18n = require("i18n");
i18n.configure({
   locales: ['en', 'kr'],
    directory: path.join(__dirname, '../', 'locales'),
    queryParameter: 'lang',
    register: global,
    directoryPermissions: '755',
    defaultLocale: 'kr',
    autoReload: true,
});

module.exports = i18n
