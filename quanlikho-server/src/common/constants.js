var path = require("path")

module.exports = {
    Role: {
        User: 'user',
        Admin: 'admin',
        Ceo: 'ceo',
        Manager: 'manager',
    },
    AuthType: {
        Kakao: 'kakao',
        Email: "email",
        Naver: 'naver',
        Facebook: 'facebook',
    },
    JWTSecret: '94rEvCERhR',
    MediaType: {
        Image: 'image',
        Video: 'video',
    },
    DateFormat: 'DD/MM/YYYY',
    TimeFormat: 'HH:mm'
}