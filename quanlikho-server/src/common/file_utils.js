var admin = require("firebase-admin");
import "@google-cloud/storage"
import { randomFileNameByExtension } from './utils'
const mime = require('mime-types');
var fs = require("fs");
var path = require("path");
var filesize = require("filesize"); 

var serviceAccount = require('./firebase/buriburi.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "gs://" + process.env.FIREBASE_STORAGE_BUCKET
});

var bucket = admin.storage().bucket();

function FileUtils() {

}

FileUtils.uploadFileToStorage = (file) => {
  return new Promise((resolve, reject) => {
    if (file) {
      try {
        var extension = path.extname(file.originalname)
        let fileUpload = bucket.file(randomFileNameByExtension(extension));
        fileUpload.save(new Buffer.from(file.buffer)).then(
          () => {
            resolve(fileUpload.name)
          }
        ).catch(reject);
      } catch (error) {
        reject(error)
      }
    } else {
      resolve(new Error(__("Empty file")))
    }
  });
}

FileUtils.deletImageStorage = (fileName) => {
  if (fileName && fileName.indexOf("http") == -1) {
    bucket.file(fileName).delete()
      .then(() => {

      })
      .catch((err) => {
        //console.log(err);
      })
  }

}

module.exports = FileUtils;
