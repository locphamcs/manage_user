const sgMail = require('@sendgrid/mail');
var url = require("url");
var path = require("path");
import _ from 'lodash'
import axios from 'axios'
const querystring = require('querystring');

sgMail.setApiKey(process.env.SENDBIRD_API_KEY);

export const response = (success = true, data = {}, message = '') => {
    if (success) {
        return {
            success,
            data
        }
    } else {
        return {
            success,
            message
        }
    }
}

export const randomFileNameByExtension = (extension) => {
    return Math.random().toString(36).substring(7) + extension;
}

export const sendMail = (to, content, callback, subject = '[Buriburi] Reset Username') => {
    const msg = {
        to: to,
        from: 'support@buriburi.com',
        subject,
        html: content,
    };
    sgMail.send(msg, false, (err, res) => {
        if (err) return callback(err, null)
        callback(null, res)
    });
}

export const getFileUrl = (fileName) =>{
    return `https://firebasestorage.googleapis.com/v0/b/${process.env.FIREBASE_STORAGE_BUCKET}/o/${fileName}?alt=media`;
}

export const getFileInfo = (image)=>{
    var fileName = null
          var fileUrl = null
          if (image.indexOf("http") > -1) {
            var parsed = url.parse(image);
            if (image == getFileUrl(path.basename(parsed.pathname))) {
              fileName = path.basename(parsed.pathname)
            }else{
              fileUrl = image
            }
          }else{
            fileName = image
          }

          return {fileName, fileUrl}
}

export const sendSMS = async (to, message)=>{
    try {
        return axios.post(`https://api.twilio.com/2010-04-01/Accounts/ACc24448266584534096bb3458dd6a2685/Messages.json`, querystring.stringify({
            "From":"+17028257594",
            "Body":message,
            "To":to
        }),{
            auth: {
                username: 'ACc24448266584534096bb3458dd6a2685',
                password: '255682973ca730381e31b1905c8f7f6b'
              }
        })
    } catch (error) {
        throw error
    }
}
