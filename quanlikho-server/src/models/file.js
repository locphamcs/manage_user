var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Constants = require('../common/constants')

var File = new Schema(
    {
        type: { type: String, default: Constants.MediaType.Image },
        file: { type: String, default: '' },
        createdAt: { type: Date, default: Date.now },
    }, {
    toObject: {
        transform: function (doc, ret, options) {
            delete ret.__v
            delete ret._id
            delete ret.createdAt
            return ret;
        }
    },
    toJSON: {
        transform: function (doc, ret, options) {
            delete ret.__v
            delete ret._id
            delete ret.createdAt
            return ret;
        }
    },
});

module.exports = mongoose.model('File', File);
