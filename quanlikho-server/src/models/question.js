var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Question = new Schema(
  {
    question: { type: String, default: "" },
    type: { type: String, default: "" },
    answer: { type: String, default:"" },
    options: [],
    createdAt: { type: Date, default: Date.now }
  },
  {
    toObject: {
      transform: function(doc, ret, options) {
        delete ret.__v;
        return ret;
      }
    },
    toJSON: {
      transform: function(doc, ret, options) {
        delete ret.__v;
        return ret;
      }
    }
  }
);

module.exports = mongoose.model("Question", Question);
