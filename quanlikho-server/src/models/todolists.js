var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var Constants = require("../common/constants");

var Todolists = new Schema(
  {
    type: { type: String, default: "" },
    date: { type: String, default:""},
    total: Number,
    todos:[
      {
        todo:{ type:String , default:""},
        did: { type:Boolean , default:false},
        checked: {type:Boolean, default:false }
      }
    ],
    createdAt: { type: Date, default: Date.now },
  },
  {
    toObject: {
      transform: function(doc, ret, options) {
        delete ret.__v;
        return ret;
      }
    },
    toJSON: {
      transform: function(doc, ret, options) {
        delete ret.__v;
        return ret;
      }
    }
  }
);

Todolists.pre('validate', function (next) {
  this.total = this.todos.length
  next();
});

module.exports = mongoose.model("Todolists", Todolists);
