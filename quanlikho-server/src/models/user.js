var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var Constants = require("../common/constants");
import { getFileUrl } from "../common/utils";

var User = new Schema(
  {
    name: { type: String, default: "" },
    email: { type: String, default: "" },
    code: { type: String, default: "" },
    date: { type: String, default: "" },
    hometown: { type: String, default: "" },
    postion: { type: String, default: "" },
    branch: { type: String, default: "" },
    mode: { type: String, default: "Không" },
    status: { type: String, default: "Đang làm việc" },
    role: { type: String, default:"" },
    password: { type: String, default: "" },
    createdAt: { type: Date, default: Date.now }
  },
  {
    toObject: {
      transform: function(doc, ret, options) {
        delete ret.salt;
        delete ret.hash;
        delete ret.__v;
        delete ret.verifyCode;
        delete ret.isRegistered
        if (
          ret.avatar &&
          ret.avatar != "" &&
          ret.avatar.indexOf("http") == -1 &&
          ret.avatar.indexOf("https") == -1
        ) {
          ret.avatar = getFileUrl(ret.avatar);
        }
        return ret;
      }
    },
    toJSON: {
      transform: function(doc, ret, options) {
        delete ret.salt;
        delete ret.hash;
        delete ret.__v;
        delete ret.verifyCode;
        delete ret.isRegistered
        if (
          ret.avatar &&
          ret.avatar != "" &&
          ret.avatar.indexOf("http") == -1 &&
          ret.avatar.indexOf("https") == -1
        ) {
          ret.avatar = getFileUrl(ret.avatar);
        }
        return ret;
      }
    }
  }
);

User.pre("save", function(next) {
  if (!this.name || this.name.length == 0) {
    this.name =
      this.username && this.username.length > 0
        ? this.username
        : this.email.split("@")[0];
  }
  next();
});
module.exports = mongoose.model("User", User);
