var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Group = new Schema(
  {
    name: { type: String, default: "" },
    description: { type: String, default: "" },
    size: { type: Number, default:0 },
    // listUser: [
    //     {
    //         user: {
    //             type: mongoose.Schema.Types.ObjectId,
    //             ref: 'User'
    //         }
    //     }
    // ],
    listUser: [{type:mongoose.Schema.Types.ObjectId, ref: "User"}],
    createdAt: { type: Date, default: Date.now }
    
  },
  {
    toObject: {
      transform: function(doc, ret, options) {
        delete ret.__v;
        ret.size = ret.listUser.length;
        return ret;
      }
    },
    toJSON: {
      transform: function(doc, ret, options) {
        delete ret.__v;
        ret.size = ret.listUser.length;
        return ret;
      }
    }
  }
);

module.exports = mongoose.model("Group", Group);
