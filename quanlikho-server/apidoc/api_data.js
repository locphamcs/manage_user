define({ "api": [
  {
    "type": "post",
    "url": "/auth/find-email",
    "title": "Find email",
    "version": "1.0.0",
    "name": "Find_email",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/find-password",
    "title": "Find password",
    "version": "1.0.0",
    "name": "Find_password",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/login",
    "title": "Login",
    "version": "1.0.0",
    "name": "Login",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "userInfo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/register",
    "title": "Register",
    "version": "1.0.0",
    "name": "Register",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/code",
    "title": "Send verify code via phone",
    "version": "1.0.0",
    "name": "Send_verify_code_via_phone",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/forgot/code",
    "title": "Send verify code via phone",
    "version": "1.0.0",
    "name": "Send_verify_code_via_phone",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/social_login",
    "title": "Social login",
    "version": "1.0.0",
    "name": "Social_login",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "nickname",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "socialId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "authType",
            "description": "<p>kakao, naver, facebook</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "avatar",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "userInfo",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/files/image",
    "title": "Upload image",
    "version": "1.0.0",
    "name": "Upload_image",
    "group": "Files",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "file",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "imageName",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/files.js",
    "groupTitle": "Files"
  },
  {
    "type": "post",
    "url": "/group",
    "title": "Add Group",
    "version": "1.0.0",
    "name": "Add_Group",
    "group": "Group",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/group.js",
    "groupTitle": "Group"
  },
  {
    "type": "post",
    "url": "/group/addMember/:id",
    "title": "Add Member",
    "version": "1.0.0",
    "name": "Add_Member",
    "group": "Group",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/group.js",
    "groupTitle": "Group"
  },
  {
    "type": "delete",
    "url": "/group/:id",
    "title": "Delete Group",
    "version": "1.0.0",
    "name": "Delete_Group",
    "group": "Group",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "filename": "src/routes/group.js",
    "groupTitle": "Group"
  },
  {
    "type": "put",
    "url": "/group/:id",
    "title": "Edit Group",
    "version": "1.0.0",
    "name": "Edit_Group",
    "group": "Group",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/group.js",
    "groupTitle": "Group"
  },
  {
    "type": "get",
    "url": "/group",
    "title": "Get Group",
    "version": "1.0.0",
    "name": "Get_Group",
    "group": "Group",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>default 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "description": "<p>default 10</p>"
          }
        ]
      }
    },
    "filename": "src/routes/group.js",
    "groupTitle": "Group"
  },
  {
    "type": "post",
    "url": "/questions",
    "title": "Add Question (Manager)",
    "version": "1.0.0",
    "name": "Add_Question",
    "group": "Question",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>text hoặc multipleChoice</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "question",
            "description": "<p>nội dung câu hỏi</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "options",
            "description": "<p>Danh sách các lựa chọn</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "answer",
            "description": "<p>Thứ tự câu trả lời đúng hoặc Đáp án nếu đó là câu hỏi Text</p>"
          }
        ]
      }
    },
    "filename": "src/routes/questions.js",
    "groupTitle": "Question"
  },
  {
    "type": "delete",
    "url": "/questions/:id",
    "title": "Delete Question",
    "version": "1.0.0",
    "name": "Delete_Question",
    "group": "Question",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "filename": "src/routes/questions.js",
    "groupTitle": "Question"
  },
  {
    "type": "put",
    "url": "/questions/:id",
    "title": "Edit Question",
    "version": "1.0.0",
    "name": "Edit_Question",
    "group": "Question",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>text hoặc multipleChoice</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "question",
            "description": "<p>nội dung câu hỏi</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "options",
            "description": "<p>Danh sách các lựa chọn</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "answer",
            "description": "<p>Thứ tự câu trả lời đúng hoặc Đáp án nếu đó là câu hỏi Text</p>"
          }
        ]
      }
    },
    "filename": "src/routes/questions.js",
    "groupTitle": "Question"
  },
  {
    "type": "get",
    "url": "/questions",
    "title": "Get Question",
    "version": "1.0.0",
    "name": "Get_Question",
    "group": "Question",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>default 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "description": "<p>default 10</p>"
          }
        ]
      }
    },
    "filename": "src/routes/questions.js",
    "groupTitle": "Question"
  },
  {
    "type": "post",
    "url": "/todolists",
    "title": "Add Todolist (Gửi list công việc)",
    "version": "1.0.0",
    "name": "Add_Todo",
    "group": "Todo_list",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>today or month</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "todos",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Mô tả công việc</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "did",
            "description": "<p>trạng thái hoàn thành, mặc định false</p>"
          }
        ]
      }
    },
    "filename": "src/routes/todolists.js",
    "groupTitle": "Todo_list"
  },
  {
    "type": "delete",
    "url": "/todolists/:id",
    "title": "Delete Todo list",
    "version": "1.0.0",
    "name": "Delete_Todo_list",
    "group": "Todo_list",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "filename": "src/routes/todolists.js",
    "groupTitle": "Todo_list"
  },
  {
    "type": "put",
    "url": "/todolists/:id/check",
    "title": "Edit Todo list (Manager kiểm tra công việc user)",
    "version": "1.0.0",
    "name": "Edit_Todo_list_(Manager_kiểm_tra_công_việc_user)",
    "group": "Todo_list",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "todos",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>_id của todo</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "checked",
            "description": "<p>trạng thái hoàn thành, mặc định false</p>"
          }
        ]
      }
    },
    "filename": "src/routes/todolists.js",
    "groupTitle": "Todo_list"
  },
  {
    "type": "put",
    "url": "/todolists/:id",
    "title": "Edit Todo list (Update trạng thái công việc của user)",
    "version": "1.0.0",
    "name": "Edit_Todo_list_(Update_trạng_thái_công_việc_của_user)",
    "group": "Todo_list",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "todos",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>_id của todo</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "did",
            "description": "<p>trạng thái hoàn thành, mặc định false</p>"
          }
        ]
      }
    },
    "filename": "src/routes/todolists.js",
    "groupTitle": "Todo_list"
  },
  {
    "type": "get",
    "url": "/todolists",
    "title": "Get todolists",
    "version": "1.0.0",
    "name": "Get_todolist",
    "group": "Todo_list",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>today hoac month</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>default 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "description": "<p>default 10</p>"
          }
        ]
      }
    },
    "filename": "src/routes/todolists.js",
    "groupTitle": "Todo_list"
  },
  {
    "type": "put",
    "url": "/users",
    "title": "Edit My Profile",
    "version": "1.0.0",
    "name": "Edit_My_Profile",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "avatar",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "code",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/phone-code",
    "title": "Send code via phone",
    "version": "1.0.0",
    "name": "Send_code_via_phone",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": ""
          }
        ]
      }
    },
    "filename": "src/routes/users.js",
    "groupTitle": "Users"
  }
] });
